package com.aplicacionesdehumor.chistes;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.google.android.gms.ads.MobileAds;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
    public String jokeFromFile(){
        String[] jokes;
        String joke;

        try {
            InputStream in = getAssets().open("jokesFileHD.txt");
            jokes = convertStreamToString(in).split("ççç");
            int seleccion = (int) (Math.random() * jokes.length);
            joke = jokes[seleccion];
        }catch(Exception e){
        	joke = "¿Qué le dice un gusano a otro?\n" +
				"Me voy a dar una vuelta a la manzana.";
        }
            return joke;
    }
	public void siguienteChiste(View view){
		TextView txtCambiado = (TextView)findViewById(R.id.textView1);
        String chiste;
		try{
            chiste = jokeFromFile();
		    txtCambiado.setText(chiste);
		}
		catch (Exception e){
			txtCambiado.setText(getString(R.string.errorSeleccionandoChiste));
		}
	}
	public void shareIt(String app) {

		TextView textoACompartir = (TextView)findViewById(R.id.textView1);
		String shareBody =(String) textoACompartir.getText();

		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
	    sharingIntent.setType("text/plain");
	    sharingIntent.setPackage(app);
	    if (sharingIntent != null) {
	    	String shareString = getString(R.string.shareSubject) + shareBody;
	        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareString);
	        startActivity(Intent.createChooser(sharingIntent, "Compartir con"));
	    }
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		int holoLightOrange = Color.parseColor(("#ffffbb33"));
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(holoLightOrange));
        MobileAds.initialize(this, "ca-app-pub-1928918027934608~7225485184");
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch(id){
			case R.id.action_settings: {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("En proceso");
                alertDialog.setMessage("Estamos trabajando en ello");
                alertDialog.show();
			return true;
		}
			case R.id.menu_item_shareWA: {
	    	    shareIt("com.whatsapp");
	    	return true;
	    }
			case R.id.menu_item_share:{
	    	    shareIt(null);
	    	return true;
	    }
            case R.id.menu_item_addJoke:{
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("En proceso");
                alertDialog.setMessage("Estamos trabajando en ello");
                alertDialog.show();
            return true;
            }
            case R.id.menu_item_about:{
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getString(R.string.aboutApp));
                alertDialog.setMessage(getString(R.string.textAboutApp));
                alertDialog.show();
            return true;
            }
		}
		return super.onOptionsItemSelected(item);
	}

}
